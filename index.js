const PORT = process.env.PORT || 3001
const urlDB = process.env.urlDB  || 'mongodb://localhost:27017/tlg'

const app = require('./server/app')
const db = require('./server/config/db')

db.openUri(urlDB)

app.listen(PORT)

console.log(`Listening on PORT ${PORT}`)
