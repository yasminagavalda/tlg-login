const mongoose = require('mongoose')
const Schema = mongoose.Schema
const collection = 'users'

var UserSchema = new Schema({
  username: String,
  password: String,
  logins: [Date],
}, { collection })

module.exports = mongoose.model('User', UserSchema)