const express = require('express')

const router = express.Router()

/* bodyParser */
const bodyParser = require('body-parser')
router.use(bodyParser.urlencoded({ extended: false }))
router.use(bodyParser.json())

const handleGetRegister = require('./handlers/handleGetRegister')
const handlePostRegister = require('./handlers/handlePostRegister')

router.get('/register', handleGetRegister)

router.post('/register', handlePostRegister)

module.exports = router