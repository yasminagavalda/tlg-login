const path = require('path')
const express = require('express')
const router = express.Router()

const User = require(path.join(__dirname, '../../../models/User'))

function handlePostRegister (req, res) {
	const {username, password} = req.body
	User.findOne({username: username})
		.then(user => {
			if (!user) {
				User.create({username, password})
				res.redirect('/login')
			} else {
				res.render('pages/register', {userexists:true})
			}
		})
}

module.exports = handlePostRegister