const express = require('express')

const router = express.Router()

const handleLanding = require('./handlers/handleLanding')

router.get('/', handleLanding)

module.exports = router
