const moment = require('moment')
const path = require('path')

const User = require(path.join(__dirname, '../../../models/User'))

function handleHome (req, res) {
	const username = req.session.username
	const password = req.session.password
	if (username && password) {
		User.find({username, password})
			.then (user => {
				if (!user) {
					res.redirect('/login')
				} else {
					User.update({username}, { '$push': { 'logins': moment() }}, function(err, data) {
						res.render('pages/home')
					})
				}
			})
	} else {
		res.redirect('/login')
	}
}

module.exports = handleHome