const express = require('express')

const router = express.Router()

const handleHome = require('./handlers/handleHome')

router.get('/home', handleHome)

module.exports = router