const express = require('express')

const router = express.Router()

/* bodyParser */
const bodyParser = require('body-parser')
router.use(bodyParser.urlencoded({ extended: false }))
router.use(bodyParser.json())

const handleGetLogin = require('./handlers/handleGetLogin')
const handlePostLogin = require('./handlers/handlePostLogin')

router.get('/login', handleGetLogin)

router.post('/login', handlePostLogin)

module.exports = router