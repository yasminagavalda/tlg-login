const path = require('path')
const express = require('express')
const cookieSession = require('cookie-session')

const router = express.Router()

const User = require(path.join(__dirname, '../../../models/User'))

function handlePostLogin (req, res) {
	const {username, password} = req.body
	User.findOne({username: username, password:password})
		.then(user => {
			if (!user) {
				res.render('pages/login', {nouser:true})
			} else {
				req.session.username = username
				req.session.password = password
				res.redirect('/home')
			}
		})
}

module.exports = handlePostLogin