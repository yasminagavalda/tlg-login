const express = require('express')
const path = require('path')
const cookieSession = require('cookie-session')

const app = express()

/* Static Path */
const publicPath = path.join(__dirname, '../public')
app.use(express.static(publicPath))

/* App settings */
app.set('views', path.resolve('server/views'))
app.set('view engine', 'pug')

/* Routes */
const registerRoute = require('./routes/register/')
const loginRoute = require('./routes/login/')
const homeRoute = require('./routes/home/')
const landingRoute = require('./routes/landing/')

app.use(cookieSession({
  name: 'UserDataSession',
  keys: ['SecretKey', 'SuperSecretKey'],
  maxAge: 120000
}))

app.use(registerRoute)
app.use(loginRoute)
app.use(homeRoute)
app.use(landingRoute)

module.exports = app
