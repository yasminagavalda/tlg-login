# Project Register & Login with DB

## Installation

To install the dependencies needed for this project you must do first of all

~~~
npm install
~~~

## Installation

You have to open mongo:
~~~
mongod
~~~
and:
~~~
mongo
~~~

## Start

To run app do 

~~~
npm start
~~~